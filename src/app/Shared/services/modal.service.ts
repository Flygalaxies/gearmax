import {
  Injectable, ViewContainerRef, ComponentFactoryResolver,
  ComponentFactory, ComponentRef, ViewChild
} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  componentRef: any;
  constructor() {
  }
  createComponent(childEntry: ViewContainerRef, component: any, resolver: ComponentFactoryResolver) {
    childEntry.clear();
    const factory = resolver.resolveComponentFactory(component);
    this.componentRef = childEntry.createComponent(factory);
    // this.componentRef.instance.office = office;    
  }
  destroyComponent() {
    this.componentRef.destroy();
  }
  setOffice(office) {
    this.componentRef.instance.officeID = office
  }


}
