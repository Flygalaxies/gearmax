import { E_TYPE } from "../enums/officeType.enum";

export class Office {

    private _type;
    private _x;
    private _y;
    private _size;

    constructor(type: E_TYPE, xPos, yPos, size) {
        this._type = type;
        this._x = xPos;
        this._y = yPos;
        this._size = size;
    }

    drawOffice(ctx: CanvasRenderingContext2D) {
        if (this._type === E_TYPE.headoffice) {
            ctx.beginPath();
            ctx.moveTo(this._x, this._y - this._size);
            ctx.lineTo(this._x - (this._size * 0.25), this._y - (this._size * 0.5));
            ctx.lineTo(this._x - (this._size), this._y - (this._size * 0.5));
            ctx.lineTo(this._x - (this._size * 0.35), this._y);
            ctx.lineTo(this._x - (this._size * 0.75), this._y + (this._size));
            ctx.lineTo(this._x, this._y + (this._size * 0.35));
            ctx.lineTo(this._x + (this._size * 0.75), this._y + (this._size));
            ctx.lineTo(this._x + (this._size * 0.35), this._y);
            ctx.lineTo(this._x + (this._size), this._y - (this._size * 0.5));
            ctx.lineTo(this._x + (this._size * 0.25), this._y - (this._size * 0.5));
            ctx.lineTo(this._x, this._y - (this._size));
            // ctx.closePath();
            ctx.fill();
            ctx.stroke();
        }
        if (this._type === E_TYPE.branch) {
            ctx.beginPath();
            ctx.arc(this._x, this._y, this._size, 0, Math.PI * 2)
            ctx.fill();
            ctx.stroke();
        }
        if (this._type === E_TYPE.distributor) {
            ctx.beginPath();
            ctx.moveTo(this._x, this._y - this._size)
            ctx.lineTo(this._x - this._size, this._y + this._size);
            ctx.lineTo(this._x + this._size, this._y + this._size);
            ctx.lineTo(this._x, this._y - this._size);
            ctx.fill();
            ctx.stroke();
        }
        if (this._type === E_TYPE.dealer) {
            ctx.beginPath();
            ctx.lineTo(this._x - this._size, this._y - this._size)
            ctx.lineTo(this._x + this._size, this._y - this._size)
            ctx.lineTo(this._x + this._size, this._y + this._size)
            ctx.lineTo(this._x - this._size, this._y + this._size)
            ctx.lineTo(this._x - this._size, this._y - this._size)
            ctx.fill();
            ctx.stroke();
        }
    }
    getMaxWidth() {
        return this._x + this._size;
    }
    getMinWidth() {
        return this._x - this._size;
    }
    getMaxHeight() {
        return this._y + this._size;
    }
    getMinHeight() {
        return this._y - this._size;
    }
}