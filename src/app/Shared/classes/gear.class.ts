export class Gear {
    private diameter;
    public radius;
    private innercircle;

    private phi0;
    public angularSpeed;
    private createdAt;

    public x;
    public y;
    _x
    _y
    _connectionRadius
    _teeth
    _fillstyle
    _strokestyle
    _text
    initGear(x, y, connectionRadius, teeth, fillstyle, strokestyle, text) {
        this.x = x;
        this.y = y;
        this.diameter = teeth * 4 * connectionRadius;
        this.radius = this.diameter / (2 * Math.PI);
        this.innercircle = this.radius / 1.5;

        this.phi0 = 0;//Starting Angle
        this.angularSpeed = 0;//Speed of rotation
        this.createdAt = new Date();//TimeStamp
    }

    constructor() { }

    drawGear(context) {
        let newDate = new Date();
        let ellapsed = newDate.getTime() - this.createdAt.getTime();
        let phiDegrees = this.angularSpeed * (ellapsed / 1000);
        let phi = this.phi0 + ((2 * Math.PI * phiDegrees) / 360);

        context.fillStyle = this._fillstyle;
        context.strokeStyle = this._strokestyle;
        context.lineCap = 'round';
        context.lineWidth = 2;

        context.save();
        // context.shadowColor = 'black';
        // context.shadowBlur = 20;
        // context.shadowOffsetX = -5;
        // context.shadowOffsetY = 10;
        context.beginPath();
        for (let i = 0; i < this._teeth * 2; i++) {
            let alpha = 2 * Math.PI * (i / (this._teeth * 2)) + phi;
            // Calculate individual touth position
            let x = this._x + Math.cos(alpha) * this.radius;
            let y = this._y + Math.sin(alpha) * this.radius;
            // Draw a half-circle, rotate it together with alpha
            // On every odd touth, invert the half-circle
            context.arc(x, y, this._connectionRadius, -Math.PI / 2 + alpha, Math.PI / 2 + alpha, i % 2 == 0);
        }
        context.fill();
        context.stroke();
        context.restore();
        //Innercircle
        context.beginPath();
        context.fillStyle = "#1f1f1f";
        context.arc(this._x, this._y, this.innercircle, 0, 2 * Math.PI, true);
        context.fill();
        context.stroke();

        //Draw text in gear
        context.save();
        context.beginPath();
        context.font = "20pt Ariel"
        context.fillStyle = "black";
        this.arcText(context, this._text, this._x, this._y, (this.radius - 30), (Math.PI * 0.6));
        context.fill();
        context.stroke();
        context.restore();
    }

    distance(x1, y1, x2, y2) {
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }
    connect(x, y, text) {
        let r = this.radius;
        let dist = this.distance(x, y, this._x, this._y);

        let newRadius = Math.max(dist - r, 10);
        let newDiam = newRadius * 2 * Math.PI;
        let newTeeth = Math.round(newDiam / (4 * this._connectionRadius));

        let actualDiameter = newTeeth * 4 * this._connectionRadius;
        let actualRadius = actualDiameter / (2 * Math.PI);
        let actualDist = r + actualRadius;
        let alpha = Math.atan2(y - this._y, x - this._x);
        let actualX = this._x + Math.cos(alpha) * actualDist;;
        let actualY = this._y + Math.sin(alpha) * actualDist;


        // let newGear = new Gear(actualX, actualY, this._connectionRadius, newTeeth, this._fillstyle, this._strokestyle, text);
        let newGear = new Gear();
        newGear.initGear(actualX, actualY, this._connectionRadius, newTeeth, this._fillstyle, this._strokestyle, text);


        let gearRatio = this._teeth / newTeeth;
        newGear.angularSpeed = -this.angularSpeed * gearRatio;

        this.phi0 = alpha + (this.phi0 - alpha);
        newGear.phi0 = alpha + (Math.PI / newTeeth) + (this.phi0 - alpha) * (newGear.angularSpeed / this.angularSpeed);
        newGear.createdAt = this.createdAt;

        return newGear;
    }
    arcText(context, str, centerX, centerY, radius, angle) {
        context.save();
        context.translate(centerX, centerY);
        context.rotate(-1 * angle / 2);
        context.rotate(-1 * (angle / str.length) / 2);
        for (let n = 0; n < str.length; n++) {
            context.rotate(angle / str.length);
            context.save();
            context.translate(0, -1 * radius);
            let char = str[n];
            context.fillText(char, 0, 0);
            context.restore();
        }
        context.restore();
    }
}