import { I_Office } from "../models/office.model";
import { E_TYPE } from "../enums/officeType.enum";
import { E_Offices } from "../enums/office.enum";

export const OfficesData: I_Office[] = [
    {
        'BranchID': 1,
        'OfficeType': 'HEAD OFFICE',
        'OfficeName': 'Johannesburg',
        'Address': 'Unit 2a, Benrose Industrial VIllage, 23 New Goch Rd, Benrose',
        'Number': '011 618 1074',
        'ContactPerson': 'Henning Nothnagel',
        'Email': 'henning@gearmax.co.za',
        'Image': '/assets/headOffice.jpg',
        'maps': 'https://www.google.co.za/maps/place/Gearmax+Parts+%26+Service+Johannesburg/@-26.21033,28.0759013,17z/data=!4m5!3m4!1s0x0:0xa9ac0b94fe0e334a!8m2!3d-26.2104671!4d28.0769529'
    },
    {
        'BranchID': 2,
        'OfficeType': 'BRANCH',
        'OfficeName': 'Johannesburg',
        'Address': 'Unit 1F, 11 - 17 New Goch Road, Benrose Johannesburg 2094',
        'Number': '011 618 1074',
        'ContactPerson': 'Arrie Joubert',
        'Email': 'arrie@gearmax.co.za',
        'Image': '/assets/jhb.jpg',
        'maps': 'https://www.google.co.za/maps/search/Unit+1F,+11+-+17+New+Goch+Road,+Benrose+Johannesburg+2094/@-26.2117499,28.0764663,17z/data=!3m1!4b1'
    },
    {
        'BranchID': 3,
        'OfficeType': 'BRANCH',
        'OfficeName': 'Pretoria',
        'Address': '213 Dykor Street, Silverton',
        'Number': '012 804 5666/7',
        'ContactPerson': 'Arno Pretorius',
        'Email': 'arno@gearmax.co.za',
        'Image': '/assets/pta.jpg',
        'maps': 'https://www.google.co.za/maps/place/213+Dykor,+Silverton,+Pretoria,+0184/@-25.731202,28.2910623,17z/data=!3m1!4b1!4m5!3m4!1s0x1e95600c7978bf4f:0xaea05d4541cba1a3!8m2!3d-25.731202!4d28.293251'
    },
    {
        'BranchID': 4,
        'OfficeType': 'BRANCH',
        'OfficeName': 'Vereeniging',
        'Address': 'Shop 4, Dickonson Lane, Duncanville',
        'Number': '016 421 1536',
        'ContactPerson': 'Lena Nel',
        'Email': 'lena@gearmax.co.za',
        'Image': '/assets/gearmaxLogo.png',
        'maps': 'https://www.google.co.za/maps/search/Dickonson+Lane,+Duncanville/@-26.6604803,27.9212766,17z/data=!3m1!4b1'
    },
    {
        'BranchID': 5,
        'OfficeType': 'BRANCH',
        'OfficeName': 'Cape Town',
        'Address': '270 Voortrekker Road, Parow',
        'Number': '021 930 6711/5',
        'ContactPerson': 'Etienne Kriel',
        'Email': 'etien.kriel@gearmax.co.za',
        'Image': '/assets/gearmaxLogo.png',
        'maps': 'https://www.google.co.za/maps/place/270+Voortrekker+Rd,+Parow+East,+Cape+Town,+7501/@-33.9049076,18.592634,17z/data=!4m5!3m4!1s0x1dcc5a674b58b4a9:0x907d346bd604f672!8m2!3d-33.90497!4d18.59493'
    },
    {
        'BranchID': 6,
        'OfficeType': 'BRANCH',
        'OfficeName': 'Nelspruit',
        'Address': 'Riverside Industrial Park, 7B Rapid Street, Nelspruit',
        'Number': '013 755 2360/3',
        'ContactPerson': 'Johan Luus',
        'Email': 'johan@gearmax.co.za',
        'Image': '/assets/nelspruit.png',
        'maps': 'https://www.google.co.za/maps/search/Riverside+Industrial+Park,+Rapid+Street/@-25.4487205,30.9563345,17z/data=!3m1!4b1'
    },
    {
        'BranchID': 7,
        'OfficeType': 'BRANCH',
        'OfficeName': 'Port Elizabeth',
        'Address': '8 Sutton Road, Sidwell',
        'Number': '041 453 1845',
        'ContactPerson': 'Ivor Vermaak',
        'Email': 'ivor@gearmax.co.za',
        'Image': '/assets/pe.png',
        'maps': 'https://www.google.co.za/maps/place/8+Sutton+Rd,+Sidwell,+Port+Elizabeth,+6061/@-33.9218199,25.5931613,17z/data=!3m1!4b1!4m5!3m4!1s0x1e7ad394785d27b3:0xab0e9d6018bc2b3c!8m2!3d-33.9218199!4d25.59535'
    },
    {
        'BranchID': 8,
        'OfficeType': 'DEALER',
        'OfficeName': 'Bloemfontein',
        'Address': '49-66 Glen Road, Hilton',
        'Number': '051 447 0081',
        'ContactPerson': 'Jonathan Baartman',
        'Email': 'jonathan.baartman@gearmax.co.za',
        'Image': '/assets/gearmaxLogo.png',
        'maps': 'https://www.google.co.za/maps/search/49-66+Glen+Road,+Hilton+++++++++++/@-29.5553484,30.2469439,13z/data=!3m1!4b1'
    },
    {
        'BranchID': 9,
        'OfficeType': 'DEALER',
        'OfficeName': 'Durban',
        'Address': '829 Umgeni Road. Umgeni',
        'Number': '031 303 2011',
        'ContactPerson': 'Akash Sooklall',
        'Email': 'akash.sooklall@gearmax.co.za',
        'Image': '/assets/gearmaxLogo.png',
        'maps': 'https://www.google.co.za/maps/place/829+Umgeni+Rd,+Windermere,+Berea,+4001/@-29.824538,31.0238363,17z/data=!3m1!4b1!4m5!3m4!1s0x1ef7077119f26c07:0x85c7b86269c77ba2!8m2!3d-29.824538!4d31.026025'
    },
    {
        'BranchID': 10,
        'OfficeType': 'DISTRIBUTOR',
        'OfficeName': 'Pietermaritzburg',
        'Address': '211 Greyling Street, Pietermaritzburg',
        'Number': '033 345 3334',
        'ContactPerson': 'Denzil Pillay',
        'Email': 'denzil@axleandtrans.co.za ',
        'Image': '/assets/gearmaxLogo.png',
        'maps': 'https://www.google.co.za/maps/place/211+Greyling+St,+Pietermaritzburg,+3201/@-29.59839,30.3698213,17z/data=!3m1!4b1!4m5!3m4!1s0x1ef6bc9749563db5:0x71deb1b27d223e86!8m2!3d-29.59839!4d30.37201'
    },
    {
        'BranchID': 11,
        'OfficeType': 'DISTRIBUTOR',
        'OfficeName': 'East London',
        'Address': '1 Dyer Road, Arcadia',
        'Number': '079 899 9755',
        'ContactPerson': 'Chris Cox',
        'Email': 'masterboxels@gmail.com',
        'Image': '/assets/gearmaxLogo.png',
        'maps': 'https://www.google.co.za/maps/place/1+Dyer+St,+Arcadia,+East+London,+5201/@-33.009141,27.9063747,17z/data=!3m1!4b1!4m5!3m4!1s0x1e66e1c43af508b7:0x8752128fa27d5856!8m2!3d-33.009141!4d27.9085634'
    }
]