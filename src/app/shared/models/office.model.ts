import { E_Offices } from "../enums/office.enum";

export interface I_Office {
    BranchID: number;
    OfficeType: string;
    OfficeName: String;
    Address: string;
    Number: string;
    ContactPerson: string;
    Email: string;
    Image?: string;
    maps?: string;
}