import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeModule } from './Modules/home/home.module';
import { InfoModule } from './Modules/info/info.module';

const routes: Routes = [
    { path: '', loadChildren: () => HomeModule },
    { path: 'info', loadChildren: () => InfoModule }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class appRoutingModule { }