import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-info',
  templateUrl: './main-info.component.html',
  styleUrls: ['./main-info.component.scss']
})
export class MainInfoComponent implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  navigate(location: string) {
    if (location === 'home') {
      this._router.navigate([''])
    }
    if (location === 'about') {
      this._router.navigate(['info/about'])
    }
    if (location === 'products') {
      this._router.navigate(['info/products'])
    }
    if(location === 'network'){
      this._router.navigate(['info/network'])
    }
  }

}
