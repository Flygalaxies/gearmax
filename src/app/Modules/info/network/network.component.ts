import { Component, OnInit } from '@angular/core';
import { OfficesData } from '../../../shared/data/offices.data';
import { I_Office } from '../../../shared/models/office.model';

@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.scss']
})
export class NetworkComponent implements OnInit {

  offices: I_Office[];

  constructor() { }

  ngOnInit() {
    this.offices = OfficesData;
    // this.initPictures();
  }

  initPictures() {
    // this.offices.forEach(element => {
      // if (element.Image === '/assets/gearmaxLogo.png') {
        // element.Image = '/assets/gearmaxG.png'
        // this.officeImage = 'data:image/png;base64,'+element.Image;        
      // }
    // })
  }

}
