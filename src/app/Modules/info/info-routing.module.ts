import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { MainInfoComponent } from './main-info/main-info.component';
import { ProductsComponent } from './products/products.component';
import { NetworkComponent } from './network/network.component';

const routes: Routes = [
  {
    path: '', component: MainInfoComponent,
    children: [
      { path: 'about', component: AboutComponent },
      { path: 'products', component: ProductsComponent },
      { path: 'network', component: NetworkComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfoRoutingModule { }
