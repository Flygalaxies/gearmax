import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from '../../Modules/info/about/about.component';
import { InfoRoutingModule } from './info-routing.module';
import { MainInfoComponent } from '../../Modules/info/main-info/main-info.component';
import { ProductsComponent } from '../../Modules/info/products/products.component';
import { NetworkComponent } from '../../Modules/info/network/network.component';

@NgModule({
  imports: [
    CommonModule,
    InfoRoutingModule
  ],
  declarations: [AboutComponent, MainInfoComponent, ProductsComponent, NetworkComponent]
})
export class InfoModule { }
