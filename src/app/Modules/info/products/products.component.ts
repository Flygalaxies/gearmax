import { Component, AfterViewInit, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Gear } from '../../../Shared/classes/gear.class';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  constructor() {    
  }

  ngOnInit() {
  }  
}
