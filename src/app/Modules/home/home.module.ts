import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from '../../Modules/home/components/home/home.component';
import { OfficeDetailsComponent } from '../../Modules/home/components/office-details/office-details.component';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule
  ],
  declarations: [HomeComponent, OfficeDetailsComponent],
  entryComponents: [OfficeDetailsComponent]
  })
export class HomeModule { }
