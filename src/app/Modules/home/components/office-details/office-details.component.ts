import { Component, OnInit, Input, HostListener } from '@angular/core';
import { E_TYPE } from '../../../../Shared/enums/officeType.enum';
import { E_Offices } from '../../../../Shared/enums/office.enum';
import { HomeComponent } from '../home/home.component';
import { ModalService } from '../../../../Shared/services/modal.service';
import { I_Office } from '../../../../shared/models/office.model';
import { OfficesData } from '../../../../shared/data/offices.data';


@Component({
  selector: 'app-office-details',
  templateUrl: './office-details.component.html',
  styleUrls: ['./office-details.component.scss']
})
export class OfficeDetailsComponent implements OnInit {
  @HostListener('click', ['$event']) click(event: MouseEvent): void {
    if (event.target === document.getElementById('modalBackground')) {
      this.close();
    }
  }
  // @Input() home: HomeComponent;
  officeImage;
  officeID;
  officeName;
  officeType;
  officeAddress;
  officeNumber;
  map;
  email;
  contactPerson;
  offices: I_Office[];

  constructor(private _modalService: ModalService) {
  }

  ngOnInit() {
    this.offices = OfficesData;
    this.initOfficeDetails();
  }

  initOfficeDetails() {
    this.offices.forEach(element => {
      if (element.BranchID === this.officeID) {
        this.officeType = element.OfficeType;
        this.officeName = element.OfficeName;
        this.officeAddress = element.Address;
        this.officeNumber = element.Number;
        this.email = element.Email;
        this.contactPerson = element.ContactPerson;
        if(element.maps){
          this.map = element.maps;
        }
        if(element.Image !== '/assets/gearmaxLogo.png'){
          this.officeImage = element.Image;        
          // this.officeImage = 'data:image/png;base64,'+element.Image;        
        }else{
          this.officeImage = '/assets/gearmaxLogo.png';
          document.getElementById('photo').style.height = '60%';      
        }
      }
    });
  }

  close() {
    this._modalService.destroyComponent();
  }
}
