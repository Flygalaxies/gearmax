import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  HostListener,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentFactory,
  ComponentRef
} from "@angular/core";
import { Router } from "@angular/router";
import { Office } from "../../../../Shared/classes/branches.class";
import { E_TYPE } from "../../../../Shared/enums/officeType.enum";
import { E_Offices } from "../../../../Shared/enums/office.enum";
import { OfficeDetailsComponent } from "../office-details/office-details.component";
import { ModalService } from "../../../../Shared/services/modal.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements AfterViewInit {
  @ViewChild("officeContainer", { read: ViewContainerRef })
  entry: ViewContainerRef;
  componentRef: any;
  @ViewChild("keyCanvas") public keyCanvas: ElementRef;
  keyCanvasEl: HTMLCanvasElement;
  keyCtx: CanvasRenderingContext2D;
  @ViewChild("mapCanvas") public mapCanvas: ElementRef;
  mapCanvasEl: HTMLCanvasElement;
  mapCtx: CanvasRenderingContext2D;
  isInside: boolean;
  offices: E_Offices;
  keyHead: Office;
  keyBranch: Office;
  keyDistributor: Office;
  keyDealer: Office;
  head: Office;
  jhb: Office;
  pretoria: Office;
  vereeniging: Office;
  capetown: Office;
  nelspruit: Office;
  pe: Office;
  bloem: Office;
  durban: Office;
  pietermaritzburg: Office;
  eastlondon: Office;
  // private _modalService: ModalService;

  @HostListener("click", ["$event"])
  click(): void {
    if (this.isInside) {
      this.createComponent(this.offices);
    }
  }
  @HostListener("mousemove", ["$event"])
  mousePos(event: MouseEvent): void {
    if (this.isInsideChecker(event.offsetX, event.offsetY, event)) {
      document.body.style.cursor = "pointer";
    } else {
      document.body.style.cursor = "auto";
    }
  }

  constructor(
    public _router: Router,
    private _resolver: ComponentFactoryResolver,
    private _modalService: ModalService
  ) {}

  ngAfterViewInit() {
    this.initCanvas();
    this.initOffices();
    this.drawKey(this.keyCtx, this.keyCanvasEl);
    this.drawMap(this.mapCtx, this.mapCanvasEl);
  }

  initCanvas() {
    let keyElem = <HTMLElement>document.querySelector(".keyContainer");
    this.keyCanvasEl = this.keyCanvas.nativeElement;
    this.keyCanvasEl.height = keyElem.offsetHeight;
    this.keyCanvasEl.width = keyElem.offsetWidth;
    this.keyCtx = this.keyCanvasEl.getContext("2d");
    let mapElem = <HTMLElement>document.querySelector(".mapContainer");
    this.mapCanvasEl = this.mapCanvas.nativeElement;
    this.mapCanvasEl.height = mapElem.offsetHeight;
    this.mapCanvasEl.width = mapElem.offsetWidth;
    this.mapCtx = this.mapCanvasEl.getContext("2d");
  }
  initOffices() {
    let elem = this.mapCanvasEl;
    let size = elem.width;
    let elemKey = this.keyCanvasEl;
    if (elem.width < 600) {
      size = size * 1.25;
    }
    this.keyHead = new Office(
      E_TYPE.headoffice,
      elemKey.width * 0.1,
      elemKey.height * 0.3,
      size * 0.035
    );
    this.keyBranch = new Office(
      E_TYPE.branch,
      elemKey.width * 0.1,
      elemKey.height * 0.5,
      size * 0.035
    );
    this.keyDistributor = new Office(
      E_TYPE.distributor,
      elemKey.width * 0.1,
      elemKey.height * 0.7,
      size * 0.035
    );
    this.keyDealer = new Office(
      E_TYPE.dealer,
      elemKey.width * 0.1,
      elemKey.height * 0.9,
      size * 0.035
    );
    this.head = new Office(
      E_TYPE.headoffice,
      elem.width * 0.67,
      elem.height * 0.35,
      size * 0.02
    );
    this.jhb = new Office(
      E_TYPE.branch,
      elem.width * 0.71,
      elem.height * 0.35,
      size * 0.02
    );
    this.pretoria = new Office(
      E_TYPE.branch,
      elem.width * 0.7,
      elem.height * 0.3,
      size * 0.02
    );
    this.vereeniging = new Office(
      E_TYPE.branch,
      elem.width * 0.65,
      elem.height * 0.4,
      size * 0.02
    );
    this.nelspruit = new Office(
      E_TYPE.branch,
      elem.width * 0.85,
      elem.height * 0.275,
      size * 0.02
    );
    this.capetown = new Office(
      E_TYPE.branch,
      elem.width * 0.125,
      elem.height * 0.9,
      size * 0.02
    );
    this.pe = new Office(
      E_TYPE.branch,
      elem.width * 0.55,
      elem.height * 0.9,
      size * 0.02
    );
    this.bloem = new Office(
      E_TYPE.dealer,
      elem.width * 0.6,
      elem.height * 0.55,
      size * 0.02
    );
    this.durban = new Office(
      E_TYPE.dealer,
      elem.width * 0.85,
      elem.height * 0.6,
      size * 0.02
    );
    this.eastlondon = new Office(
      E_TYPE.distributor,
      elem.width * 0.675,
      elem.height * 0.825,
      size * 0.02
    );
    this.pietermaritzburg = new Office(
      E_TYPE.distributor,
      elem.width * 0.8,
      elem.height * 0.55,
      size * 0.02
    );
  }

  drawKey(ctx: CanvasRenderingContext2D, elem: HTMLCanvasElement) {
    ctx.lineWidth = 1;
    ctx.strokeStyle = "red";
    this.keyHead.drawOffice(ctx);
    this.keyBranch.drawOffice(ctx);
    this.keyDistributor.drawOffice(ctx);
    this.keyDealer.drawOffice(ctx);
    this.drawText(
      ctx,
      elem.width * 0.0,
      elem.height * 0.125,
      elem.width * 0.075,
      "NATIONWIDE COVERAGE"
    );
    this.drawText(
      ctx,
      elem.width * 0.25,
      elem.height * 0.325,
      elem.width * 0.04,
      "HEAD OFFICE"
    );
    this.drawText(
      ctx,
      elem.width * 0.25,
      elem.height * 0.525,
      elem.width * 0.04,
      "BRANCHES"
    );
    this.drawText(
      ctx,
      elem.width * 0.25,
      elem.height * 0.725,
      elem.width * 0.04,
      "DISTRIBUTORS"
    );
    this.drawText(
      ctx,
      elem.width * 0.25,
      elem.height * 0.925,
      elem.width * 0.04,
      "DEALERS"
    );
  }
  drawMap(ctx: CanvasRenderingContext2D, elem: HTMLCanvasElement) {
    ctx.strokeStyle = "#FEBEC1";
    ctx.lineWidth = 1;
    this.head.drawOffice(ctx);
    this.jhb.drawOffice(ctx);
    this.pretoria.drawOffice(ctx);
    this.vereeniging.drawOffice(ctx);
    this.nelspruit.drawOffice(ctx);
    this.capetown.drawOffice(ctx);
    this.pe.drawOffice(ctx);
    this.bloem.drawOffice(ctx);
    this.durban.drawOffice(ctx);
    this.eastlondon.drawOffice(ctx);
    this.pietermaritzburg.drawOffice(ctx);
  }
  isInsideOffice(office: Office, x, y, event: Event): boolean {    
    if (event.target === this.mapCanvasEl) {
      if (
        x > office.getMinWidth() &&
        x < office.getMaxWidth() &&
        y > office.getMinHeight() &&
        y < office.getMaxHeight()
      ) {
        return true;
      }
    }
    return false;
  }
  isInsideChecker(x, y, event: Event) {    
    if (this.isInsideOffice(this.head, x, y, event)) {
      this.isInside = true;
      this.offices = E_Offices.HEAD;
    } else if (this.isInsideOffice(this.jhb, x, y, event)) {
      this.isInside = true;
      this.offices = E_Offices.JHB;
    } else if (this.isInsideOffice(this.pretoria, x, y, event)) {
      this.isInside = true;
      this.offices = E_Offices.PRETORIA;
    } else if (this.isInsideOffice(this.vereeniging, x, y, event)) {
      this.isInside = true;
      this.offices = E_Offices.VEREENIGING;
    } else if (this.isInsideOffice(this.nelspruit, x, y, event)) {
      this.isInside = true;
      this.offices = E_Offices.NELSPRUIT;
    } else if (this.isInsideOffice(this.capetown, x, y, event)) {
      this.isInside = true;
      this.offices = E_Offices.CAPETOWN;
    } else if (this.isInsideOffice(this.pe, x, y, event)) {
      this.isInside = true;
      this.offices = E_Offices.PE;
    } else if (this.isInsideOffice(this.bloem, x, y, event)) {
      this.isInside = true;
      this.offices = E_Offices.BLOEM;
    } else if (this.isInsideOffice(this.durban, x, y, event)) {
      this.isInside = true;
      this.offices = E_Offices.DURBAN;
    } else if (this.isInsideOffice(this.eastlondon, x, y, event)) {
      this.isInside = true;
      this.offices = E_Offices.EASTLONDON;
    } else if (this.isInsideOffice(this.pietermaritzburg, x, y, event)) {
      this.isInside = true;
      this.offices = E_Offices.PIETERMARITZBURG;
    } else {
      this.isInside = false;
      this.offices = E_Offices.DEFAULT;
    }
    return this.isInside;
    // }
  }

  drawText(ctx: CanvasRenderingContext2D, x, y, size, text) {
    ctx.font = `${size}px Arial`;
    ctx.strokeText(text, x, y);
  }

  createComponent(office: E_Offices) {
    this._modalService.createComponent(
      this.entry,
      OfficeDetailsComponent,
      this._resolver
    );
    this._modalService.setOffice(this.offices);
  }
  navigate(location: string) {
    if (location === "about") {
      this._router.navigate(["/info/about"]);
    }
    if (location === "products") {
      this._router.navigate(["/info/products"]);
    }
  }
}
